import Requester from '../utils/Requester';

export default {
  getDistributors: async () => {
    const response = await Requester.get('/distributors');
    console.log('api getDistributors', response);
    if (response.ok) {
      return {
        success: true,
        distributors: response.data
      };
    }
    return { success: false };
  },
  createDistributor: async ({ name }) => {
    const response = await Requester.post('/distributor', { name });
    console.log('api createDistributor', response);
    if (response.ok) {
      return {
        success: true,
        distributor: response.data
      };
    }
    return { success: false };
  },
  getDataByDistributorId: async ({ id }) => {
    const response = await Requester.get(`/distributor_data/${id}`);
    console.log('api getDataByDistributorId', response);
    if (response.ok) {
      return {
        success: true,
        pharmacies: response.data.pharmacies,
        products: response.data.products
      };
    }
    return { success: false };
  },
  importData: async ({ distributor, file }) => {
    console.log('api importData', distributor, file);
    // eslint-disable-next-line no-undef
    const data = new FormData();
    data.append('distributor', distributor);
    data.append('file', file);
    const response = await Requester.post('/upload', data);
    if (response.ok) {
      return {
        success: true,
        ...response.data
      };
    }
    return { success: false };
  }
};
