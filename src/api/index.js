import products from './products';
import shipment from './shipment';
import pharmacies from './pharmacies';
import distributors from './distributors';
import systemProducts from './systemProducts';
import systemPharmacies from './systemPharmacies';

export default {
  products,
  shipment,
  pharmacies,
  distributors,
  systemProducts,
  systemPharmacies
};
