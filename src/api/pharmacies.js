import Requester from '../utils/Requester';

export default {
  getPharmaciesByDistributor: async (id) => {
    const response = await Requester.get(`/pharmacies/${id}`);
    console.log('api getPharmaciesByDistributor', response);
    if (response.ok) {
      return {
        success: true,
        pharmacies: response.data
      };
    }
    return { success: false };
  },
  updatePharmacy: async ({ id, systemPharmacy }) => {
    const response = await Requester.put(`/pharmacy/${id}`, { id, systemPharmacy });
    if (response.ok) {
      return {
        success: true,
        pharmacy: response.data

      };
    }
    return { success: false };
  },
};
