import Requester from '../utils/Requester';

export default {
  getProducts: async () => {
    const response = await Requester.get('/products');
    console.log('response1', response);
    if (response.ok) {
      return {
        success: true,
        products: response.data

      };
    }
    return { success: false };
  },
  updateProduct: async ({ id, systemProduct }) => {
    const response = await Requester.put(`/product/${id}`, { id, systemProduct });
    if (response.ok) {
      return {
        success: true,
        product: response.data

      };
    }
    return { success: false };
  },
};
