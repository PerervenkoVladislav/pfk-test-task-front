import Requester from '../utils/Requester';

export default {
  importShipment: async ({ distributor, shipments }) => {
    console.log('api importShipment', distributor, shipments);
    const response = await Requester.post('/import_shipments', { distributor, shipments });
    if (response.ok) {
      return {
        success: true,
        ...response.data
      };
    }
    return { success: false };
  },
  getShipments: async ({ distributor, systemProduct, systemPharmacy, page }) => {
    console.log('getShipments', distributor, systemProduct, systemPharmacy, page);
    // eslint-disable-next-line prefer-template
    const response = await Requester.get('/shipments?' +
      'page=' + (page || '') +
      '&distributor=' + (distributor || '') +
      '&systemProduct=' + (systemProduct || '') +
      '&systemPharmacy=' + (systemPharmacy || ''));
    if (response.ok) {
      return {
        success: true,
        shipments: response.data.shipments,
        count: response.data.count
      };
    }
    return { success: false };
  }
};
