import Requester from '../utils/Requester';

export default {
  getSystemPharmacies: async () => {
    const response = await Requester.get('/system_pharmacies');
    console.log('api getSystemPharmacies', response);
    if (response.ok) {
      return {
        success: true,
        systemPharmacies: response.data
      };
    }
    return { success: false };
  },
  createSystemPharmacy: async ({ name }) => {
    const response = await Requester.post('/system_pharmacy', { name });
    console.log('api createSystemPharmacy', response);
    if (response.ok) {
      return {
        success: true,
        systemPharmacy: response.data
      };
    }
    return { success: false };
  }
};
