import Requester from '../utils/Requester';

export default {
  getSystemProducts: async () => {
    const response = await Requester.get('/system_products');
    console.log('api getSystemProducts', response);
    if (response.ok) {
      return {
        success: true,
        systemProducts: response.data
      };
    }
    return { success: false };
  },
  createSystemProduct: async ({ name }) => {
    const response = await Requester.post('/system_product', { name });
    console.log('api createSystemProduct', response);
    if (response.ok) {
      return {
        success: true,
        systemProduct: response.data
      };
    }
    return { success: false };
  }
};
