import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'semantic-ui-css/semantic.min.css';
import { persistStore } from 'redux-persist';
import { ConnectedRouter as Router } from 'react-router-redux';
// import { Route, Link } from 'react-router-dom';

import configureStore, { history } from './redux/store';
import Main from './containers/Main';

const store = configureStore();
persistStore(store, { blacklist: ['router'] });

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Main />
    </Router>
  </Provider>,
  document.getElementById('container')
);
