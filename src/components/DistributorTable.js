import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

class DistributorTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      props: {
        distributors
      }
    } = this;

    if (distributors.length === 0) {
      return (<div
        style={{
          width: '100%',
          display: 'flex',
          justifyContent: 'center'
        }}
      >
        data not found
      </div>);
    }

    return (
      <div>
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell textAlign="center">Id</Table.HeaderCell>
              <Table.HeaderCell textAlign="center">Name</Table.HeaderCell>
              <Table.HeaderCell textAlign="center">Date create</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {
              distributors.map(distributor => (<Table.Row
                key={distributor.id}
              >
                <Table.Cell
                  textAlign="center"
                >
                  <Link
                    to={`/distributors/${distributor.id}`}
                  >
                    {distributor.id}
                  </Link>
                </Table.Cell>
                <Table.Cell
                  textAlign="center"
                >
                  {distributor.name}
                </Table.Cell>
                <Table.Cell
                  textAlign="center"
                >
                  {distributor.date_create}
                </Table.Cell>
              </Table.Row>))
            }
          </Table.Body>
        </Table>
      </div>
    );
  }
}

DistributorTable.propTypes = {
  distributors: PropTypes.array
};

DistributorTable.defaultProps = {
  distributors: []
};

const stateToProps = state => ({
  distributors: state.distributors
});

const actionToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(stateToProps, actionToProps)(DistributorTable);
