import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Button, Modal } from 'semantic-ui-react';

class ModalWindow extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      props: {
        title,
        show,
        closeModal,
        onSave,
        children
      }
    } = this;

    return (
      <Modal size="tiny" open={show} onClose={closeModal}>
        <Modal.Header>
          {title}
        </Modal.Header>
        <Modal.Content>
          {children}
        </Modal.Content>
        <Modal.Actions>
          <Button
            onClick={closeModal}
            negative
          >
            Cancel
          </Button>
          <Button
            onClick={onSave}
            positive
          >
            Save
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

ModalWindow.propTypes = {
  title: PropTypes.string,
  children: PropTypes.element,
  show: PropTypes.bool,
  closeModal: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired
};

const stateToProps = () => ({});

const actionToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(stateToProps, actionToProps)(ModalWindow);
