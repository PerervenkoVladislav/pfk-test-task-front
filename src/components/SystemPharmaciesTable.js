import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'semantic-ui-react';

class SystemPharmaciesTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      props: {
        systemPharmacies
      }
    } = this;

    if (systemPharmacies.length === 0) {
      return (<div
        style={{
          width: '100%',
          display: 'flex',
          justifyContent: 'center'
        }}
      >
        data not found
      </div>);
    }

    return (
      <div>
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell textAlign="center">Id</Table.HeaderCell>
              <Table.HeaderCell textAlign="center">Name</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {
              systemPharmacies.map(systemPharmacy => (<Table.Row
                key={systemPharmacy.id}
              >
                <Table.Cell
                  textAlign="center"
                >
                  {systemPharmacy.id}
                </Table.Cell>
                <Table.Cell
                  textAlign="center"
                >
                  {systemPharmacy.name}
                </Table.Cell>
              </Table.Row>))
            }
          </Table.Body>
        </Table>
      </div>
    );
  }
}

SystemPharmaciesTable.propTypes = {
  systemPharmacies: PropTypes.array
};

SystemPharmaciesTable.defaultProps = {
  systemPharmacies: []
};

const stateToProps = state => ({
  systemPharmacies: state.systemPharmacies
});

const actionToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(stateToProps, actionToProps)(SystemPharmaciesTable);
