import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'semantic-ui-react';

class SystemProductsTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      props: {
        systemProducts
      },
    } = this;

    if (systemProducts.length === 0) {
      return (<div
        style={{
          width: '100%',
          display: 'flex',
          justifyContent: 'center'
        }}
      >
        data not found
      </div>);
    }

    return (
      <div>
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell textAlign="center">Id</Table.HeaderCell>
              <Table.HeaderCell textAlign="center">Name</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {
              systemProducts.map(systemProduct => (<Table.Row
                key={systemProduct.id}
              >
                <Table.Cell
                  textAlign="center"
                >
                  {systemProduct.id}
                </Table.Cell>
                <Table.Cell
                  textAlign="center"
                >
                  {systemProduct.name}
                </Table.Cell>
              </Table.Row>))
            }
          </Table.Body>
        </Table>
      </div>
    );
  }
}

SystemProductsTable.propTypes = {
  systemProducts: PropTypes.array
};

SystemProductsTable.defaultProps = {
  systemProducts: []
};

const stateToProps = state => ({
  systemProducts: state.systemProducts
});

const actionToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(stateToProps, actionToProps)(SystemProductsTable);
