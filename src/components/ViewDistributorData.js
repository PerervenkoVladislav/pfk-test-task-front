import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Segment, Tab } from 'semantic-ui-react';

import ViewDistributorProducts from './ViewDistributorProducts';
import ViewDistributorPharmacies from './ViewDistributorPharmacies';

class ViewDistributorData extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      props: {
        id
      }
    } = this;

    return (
      <div>
        <Segment inverted>
          Distributor {id}
        </Segment>
        <Tab
          menu={{ secondary: true, pointing: true }}
          panes={[
            {
              menuItem: 'Products',
              render: () => (<Tab.Pane
                attached={false}
              >
                <ViewDistributorProducts
                  id={id}
                />
              </Tab.Pane>)
            },
            {
              menuItem: 'Pharmacies',
              render: () => (<Tab.Pane
                attached={false}
              >
                <ViewDistributorPharmacies
                  id={id}
                />
              </Tab.Pane>)
            },
          ]}
        />
      </div>
    );
  }
}

ViewDistributorData.propTypes = {
  id: PropTypes.string,
  pharmacies: PropTypes.object,
  systemPharmacies: PropTypes.array
};

ViewDistributorData.defaultProps = {};

const stateToProps = state => ({
  pharmacies: state.pharmacies,
  systemPharmacies: state.systemPharmacies
});

const actionToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(stateToProps, actionToProps)(ViewDistributorData);
