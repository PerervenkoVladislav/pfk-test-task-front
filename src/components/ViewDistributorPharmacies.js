import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table, Dropdown } from 'semantic-ui-react';

import {
  updatePharmacy
} from '../redux/actions/pharmacies';

class ViewDistributorPharmacies extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dropDownValues: {}
    };
  }

  dropDownSystemPharmacy = (pharmacy) => {
    console.log('pharmacy: ', pharmacy);
    const {
      props: {
        systemPharmacies
      }
    } = this;

    const systemProductsOptions = systemPharmacies.map(systemPharmacy => ({
      key: systemPharmacy.id,
      value: systemPharmacy.id,
      text: systemPharmacy.name
    }));

    return (
      <Dropdown
        onChange={(e, data) => {
          const id = data.value;
          this.props.updatePharmacy({ id: pharmacy.id, systemPharmacy: id });
        }}
        placeholder="System Pharmacy"
        fluid
        search
        selection
        value={pharmacy.system_pharmacy && pharmacy.system_pharmacy.id}
        options={systemProductsOptions}
      />
    );
  };

  render() {
    const {
      props: {
        id,
        pharmacies
      }
    } = this;


    console.log('pharmacies', pharmacies);
    console.log('id', id);
    return (
      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell textAlign="center">Id</Table.HeaderCell>
            <Table.HeaderCell textAlign="center">Pharmacy</Table.HeaderCell>
            <Table.HeaderCell textAlign="center">System Pharmacy</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {
            pharmacies && pharmacies[id] && pharmacies[id].map(pharmacy => (<Table.Row
              key={pharmacy.id}
            >
              <Table.Cell
                textAlign="center"
              >
                {pharmacy.id}
              </Table.Cell>
              <Table.Cell
                textAlign="center"
              >
                {pharmacy.name}
              </Table.Cell>
              <Table.Cell
                textAlign="center"
              >
                {this.dropDownSystemPharmacy(pharmacy)}
              </Table.Cell>
            </Table.Row>))
          }
        </Table.Body>
      </Table>
    );
  }
}

ViewDistributorPharmacies.propTypes = {
  id: PropTypes.string,
  pharmacies: PropTypes.object,
  systemPharmacies: PropTypes.array,
  updatePharmacy: PropTypes.func.isRequired
};

ViewDistributorPharmacies.defaultProps = {};

const stateToProps = state => ({
  pharmacies: state.pharmacies,
  systemPharmacies: state.systemPharmacies
});

const actionToProps = dispatch => bindActionCreators({
  updatePharmacy
}, dispatch);

export default connect(stateToProps, actionToProps)(ViewDistributorPharmacies);
