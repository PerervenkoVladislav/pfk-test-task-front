import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table, Dropdown } from 'semantic-ui-react';

import {
  updateProduct
} from '../redux/actions/products';

class ViewDistributorProducts extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dropDownValues: {}
    };
  }

  dropDownSystemProduct = (product) => {
    const {
      props: {
        systemProducts
      }
    } = this;

    const systemProductsOptions = systemProducts.map(systemProduct => ({
      key: systemProduct.id,
      value: systemProduct.id,
      text: systemProduct.name
    }));

    return (
      <Dropdown
        onChange={(e, data) => {
          const id = data.value;
          this.props.updateProduct({ id: product.id, systemProduct: id });
        }}
        placeholder="System Products"
        fluid
        search
        selection
        value={product.system_product && product.system_product.id}
        options={systemProductsOptions}
      />
    );
  };

  render() {
    const {
      props: {
        id,
        products
      }
    } = this;

    return (
      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell textAlign="center">Id</Table.HeaderCell>
            <Table.HeaderCell textAlign="center">Product</Table.HeaderCell>
            <Table.HeaderCell textAlign="center">System Product</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {
            products[id] && products[id].map(product => (<Table.Row
              key={product.id}
            >
              <Table.Cell
                textAlign="center"
              >
                {product.id}
              </Table.Cell>
              <Table.Cell
                textAlign="center"
              >
                {product.name}
              </Table.Cell>
              <Table.Cell
                textAlign="center"
              >
                {this.dropDownSystemProduct(product)}
              </Table.Cell>
            </Table.Row>))
          }
        </Table.Body>
      </Table>
    );
  }
}

ViewDistributorProducts.propTypes = {
  id: PropTypes.string,
  products: PropTypes.object,
  systemProducts: PropTypes.array,
  updateProduct: PropTypes.func.isRequired
};

ViewDistributorProducts.defaultProps = {};

const stateToProps = state => ({
  products: state.products,
  systemProducts: state.systemProducts
});

const actionToProps = dispatch => bindActionCreators({
  updateProduct
}, dispatch);

export default connect(stateToProps, actionToProps)(ViewDistributorProducts);
