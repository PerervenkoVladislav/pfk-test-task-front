import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { Button, Input } from 'semantic-ui-react';

import DistributorTable from '../components/DistributorTable';
import ViewDistributorData from '../components/ViewDistributorData';

import ModalWindow from '../components/ModalWindow';

import {
  createDistributor
} from '../redux/actions/distributors';

class Distributors extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
      newDistributorName: null
    };
  }

  onSave = () => {
    const {
      newDistributorName
    } = this.state;
    this.props.createDistributor(newDistributorName);
    this.setState({
      showModal: false,
      newDistributorName: null
    });
  };

  closeModal = () => {
    const {
      showModal
    } = this.state;

    if (!showModal) return;
    this.setState({ showModal: false });
  };

  render() {
    const {
      props: {
        match
      },
      state: {
        showModal
      }
    } = this;

    if (match.params.id) {
      return (
        <div>
          <ViewDistributorData
            id={match.params.id}
          />
        </div>
      );
    }
    return (
      <div>
        <div
          style={{
            width: '100%',
            display: 'flex',
            justifyContent: 'flex-end',
            padding: 10
          }}
        >
          <Button
            onClick={() => {
              if (showModal) return;
              this.setState({ showModal: true });
            }}
          >
            Add
          </Button>
        </div>
        <DistributorTable />
        <ModalWindow
          title="Create new distributor"
          closeModal={this.closeModal}
          show={showModal}
          onSave={this.onSave}
        >
          <Input
            fluid
            placeholder="Distributor name"
            onChange={(event) => {
              const value = event.target.value;
              this.setState({
                newDistributorName: value
              });
            }}
          />
        </ModalWindow>
      </div>
    );
  }
}

Distributors.propTypes = {
  match: PropTypes.object,
  createDistributor: PropTypes.func.isRequired
};

Distributors.defaultProps = {};

const stateToProps = state => ({});

const actionToProps = dispatch => bindActionCreators({
  createDistributor
}, dispatch);

export default connect(stateToProps, actionToProps)(Distributors);
