import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Segment, Table, Dropdown } from 'semantic-ui-react';

import API from '../api';

import {
  importData,
  updateProductImportData,
  updatePharmacyImportData
} from '../redux/actions/distributors';

import {
  importShipment
} from '../redux/actions/shipment';

class ImportData extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      file: null,
      shipment: [],
      distributorSelect: null,
      showProducts: {},
      error: null
    };
  }

  readFile = (event) => {
    this.setState({
      file: event.target.files[0]
    });
  };

  dropDownDistributors = () => {
    const {
      state: {
        distributorSelect
      },
      props: {
        distributors
      }
    } = this;

    const distributorsOptions = distributors.map(systemPharmacy => ({
      key: systemPharmacy.id,
      value: systemPharmacy.id,
      text: systemPharmacy.name
    }));

    return (
      <Dropdown
        onChange={(e, data) => {
          console.log('data.value', data.value);
          this.setState({
            distributorSelect: data.value,
            error: null
          });
        }}
        placeholder="Distributors"
        search
        selection
        value={distributorSelect}
        options={distributorsOptions}
      />
    );
  };

  dropDownSystemPharmacy = (pharmacy) => {
    const {
      state: {
        distributorSelect
      },
      props: {
        systemPharmacies
      }
    } = this;

    const systemProductsOptions = systemPharmacies.map(systemPharmacy => ({
      key: systemPharmacy.id,
      value: systemPharmacy.id,
      text: systemPharmacy.name
    }));

    return (
      <Dropdown
        onChange={(e, data) => {
          this.props.updatePharmacyImportData({
            distributor: distributorSelect,
            pharmacy: pharmacy.pharmacy,
            systemPharmacy: systemPharmacies[data.value - 1]
          });
        }}
        placeholder="System Products"
        search
        selection
        value={pharmacy.systemPharmacy && pharmacy.systemPharmacy.id}
        options={systemProductsOptions}
      />
    );
  };

  dropDownSystemProduct = (product) => {
    const {
      state: {
        distributorSelect
      },
      props: {
        systemProducts
      }
    } = this;

    const systemProductsOptions = systemProducts.map(systemProduct => ({
      key: systemProduct.id,
      value: systemProduct.id,
      text: systemProduct.name
    }));

    return (
      <Dropdown
        onChange={(e, data) => {
          this.props.updateProductImportData({
            distributor: distributorSelect,
            product: product.current,
            systemProduct: systemProducts[data.value - 1]
          });
        }}
        placeholder="System Products"
        fluid
        search
        selection
        value={product.system && product.system.id}
        options={systemProductsOptions}
      />
    );
  };

  importData = () => {
    const {
      state: {
        distributorSelect: distributor,
        file
      }
    } = this;

    this.props.importData({ distributor, file });
  };

  setError = (error) => {
    this.setState({
      error
    });
  };

  sendData = () => {
    const {
      state: {
        distributorSelect
      },
      props: {
        shipments
      }
    } = this;

    // {"count":65,"systemProduct":1,"systemPharmacy":1}
    const requestData = [];
    const shipment = shipments[distributorSelect];

    for (let i = 0; i < shipment.length; i += 1) {
      const ship = shipment[i];
      if (!ship.systemPharmacy) {
        return this.setError(`У аптеки ${ship.pharmacy} нет связи в сиситеме`);
      }

      for (let j = 0; j < ship.products.length; j += 1) {
        const product = ship.products[j];
        if (!product.system) {
          return this.setError(`У продукта "${product.current}" в аптеке "${ship.pharmacy}" нет связи в сиситеме`);
        }

        if (!product.system.id || !ship.systemPharmacy.id) return this.setError('Что то пошло не так!');
        requestData.push({
          count: product.count || 0,
          systemProduct: product.system.id,
          systemPharmacy: ship.systemPharmacy.id
        });
      }
    }

    if (!requestData.length) {
      return this.setError('Нет данных для отправки');
    }

    this.props.importShipment({ distributor: distributorSelect, shipments: JSON.stringify(requestData) });
  };

  render() {
    const {
      state: {
        distributorSelect,
        file,
        showProducts,
        error
      },
      props: {
        shipments
      }
    } = this;

    return (
      <div>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
        >
          {this.dropDownDistributors()}
          <input
            id="upload"
            ref="upload"
            type="file"
            // accept="image/*"
            onChange={(event) => {
              this.readFile(event);
            }}
            onClick={(event) => {
              event.target.value = null;
            }}

          />
          <Button
            disabled={!file || !distributorSelect}
            onClick={this.importData}
          >
           Import
          </Button>
        </div>
        {
          error &&
          <div
            style={{
              margin: 10,
              display: 'flex',
              color: 'red'
            }}
          >
            {error}
          </div>
        }
        <div>
          {
            distributorSelect && shipments[distributorSelect] && shipments[distributorSelect].map((sh, i) => (
              <div
                style={{
                  marginTop: 15,
                  cursor: 'pointer'
                }}
                key={`${sh.pharmacy}`}
              >
                <Segment
                  onClick={() => {
                    this.setState({
                      showProducts: {
                        ...showProducts,
                        [distributorSelect]: {
                          ...showProducts[distributorSelect],
                          [i]: showProducts[distributorSelect] ?
                            !showProducts[distributorSelect][i]
                            : true
                        }
                      }
                    });
                  }}
                  style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                  }}
                  inverted
                >
                    Pharmacy: {sh.pharmacy} {this.dropDownSystemPharmacy(sh)}
                </Segment>
                { distributorSelect
                && showProducts[distributorSelect]
                && showProducts[distributorSelect][i] &&
                  <Table celled>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell textAlign="center">Product</Table.HeaderCell>
                        <Table.HeaderCell textAlign="center">System Product</Table.HeaderCell>
                        <Table.HeaderCell textAlign="center">Count</Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>

                    <Table.Body>
                      {
                        sh.products.map(product => (<Table.Row
                          key={product.current}
                        >
                          <Table.Cell
                            textAlign="center"
                          >
                            {product.current}
                          </Table.Cell>
                          <Table.Cell
                            textAlign="center"
                          >
                            {this.dropDownSystemProduct(product)}
                          </Table.Cell>
                          <Table.Cell
                            textAlign="center"
                          >
                            {product.count}
                          </Table.Cell>
                        </Table.Row>))
                      }
                    </Table.Body>
                  </Table>
                }
              </div>
            ))
          }
        </div>
        { distributorSelect && shipments[distributorSelect] &&
          <Button
            style={{
              position: 'fixed',
              left: '2%',
              top: '94%',
              // marginLeft: -300
            }}
            color="green"
            onClick={this.sendData}
          >
            Send Data
          </Button>
        }
      </div>
    );
  }
}

ImportData.propTypes = {
  shipments: PropTypes.object,
  distributors: PropTypes.array,
  systemProducts: PropTypes.array,
  systemPharmacies: PropTypes.array,
  importData: PropTypes.func.isRequired,
  updateProductImportData: PropTypes.func.isRequired,
  updatePharmacyImportData: PropTypes.func.isRequired,
  importShipment: PropTypes.func.isRequired
};

ImportData.defaultProps = {};

const stateToProps = state => ({
  shipments: state.importData,
  distributors: state.distributors,
  systemProducts: state.systemProducts,
  systemPharmacies: state.systemPharmacies
});

const actionToProps = dispatch => bindActionCreators({
  importData,
  updateProductImportData,
  updatePharmacyImportData,
  importShipment
}, dispatch);

export default connect(stateToProps, actionToProps)(ImportData);
