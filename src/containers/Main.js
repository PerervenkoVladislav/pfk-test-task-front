import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Menu } from 'semantic-ui-react';
import { withRouter, Link } from 'react-router-dom';
import { ConnectedRouter as Router } from 'react-router-redux';

import Navigator from './Navigator';
// import Content from './Content';

import settings from '../settings';

import {
  getProducts
} from '../redux/actions/products';

import {
  setActiveItemMenu
} from '../redux/actions/application';

class Main extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      props: {
        history,
        setActiveItemMenu,
        application: {
          activeItemMenu
        }
      }
    } = this;

    return (
      <div>
        <div
          style={{
            display: 'flex',
            justifyContent: 'row'
          }}
        >
          <div
            style={{
              height: '100vh'
            }}
          >
            <Menu pointing secondary vertical>
              {
                settings.itemsMenu.map(item => (<Link
                  key={item.item}
                  to={`/${item.link}`}
                >
                  <Menu.Item
                    name={item.name}
                    active={activeItemMenu === item.item}
                    onClick={() => {
                      setActiveItemMenu({
                        activeItemMenu: item.item
                      });
                    }}
                  />
                </Link>))
              }
            </Menu>
          </div>
          <div
            style={{
              width: '100%',
              padding: '10px'
            }}
          >
            <div
              id="top-menu"
              style={{
                width: '100%',
                height: '50px',
                border: '4px solid black',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              Тут немного места для рекламы.
            </div>
            <div
              style={{
                padding: '10px'
              }}
            >
              <Navigator />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Main.propTypes = {
  match: PropTypes.object,
  application: PropTypes.object,
  getProducts: PropTypes.func.isRequired,
  setActiveItemMenu: PropTypes.func.isRequired
};

const stateToProps = state => ({
  application: state.application
});

const actionToProps = dispatch => bindActionCreators({
  getProducts,
  setActiveItemMenu
}, dispatch);

export default withRouter(connect(stateToProps, actionToProps)(Main));
