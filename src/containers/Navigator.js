import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Shipment from './Shipment';
import Distributors from './Distributors';
import SystemProducts from './SystemProducts';
import SystemPharmacies from './SystemPharmacies';
import ImportData from './ImportData';

class Navigator extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Shipment} />
        <Route path="/distributors/:id?" component={Distributors} />
        <Route path="/system_products" component={SystemProducts} />
        <Route path="/system_pharmacy" component={SystemPharmacies} />
        <Route path="/import_data" component={ImportData} />
      </Switch>
    );
  }
}

Navigator.propTypes = {};

export default Navigator;

