import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table, Menu, Button, Dropdown } from 'semantic-ui-react';
import Moment from 'moment';

import API from '../api';

class Shipment extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      shipments: [],
      countPage: 0,
      activeItem: 0,
      distributorSelect: null,
      systemProductSelect: null,
      systemPharmacySelect: null,
    };
  }

  componentWillMount = async () => {
    await this.searchShipment({});
  };

  searchShipment = async ({ distributor, systemProduct, systemPharmacy, page }) => {
    const response = await API.shipment.getShipments({ distributor, systemProduct, systemPharmacy, page });
    if (response.success) {
      this.setState({
        shipments: response.shipments,
        countPage: Math.ceil(response.count / 15)
      });
    }
  };

  dropDownDistributors = () => {
    const {
      state: {
        distributorSelect
      },
      props: {
        distributors
      }
    } = this;

    const distributorsOptions = distributors.map(systemPharmacy => ({
      key: systemPharmacy.id,
      value: systemPharmacy.id,
      text: systemPharmacy.name
    }));

    return (
      <Dropdown
        style={{
          margin: '0 10px'
        }}
        onChange={(e, data) => {
          this.setState({
            distributorSelect: data.value
          });
        }}
        placeholder="Distributors"
        search
        selection
        value={distributorSelect}
        options={distributorsOptions}
      />
    );
  };

  dropDownSystemPharmacy = () => {
    const {
      state: {
        systemPharmacySelect
      },
      props: {
        systemPharmacies
      }
    } = this;

    const systemProductsOptions = systemPharmacies.map(systemPharmacy => ({
      key: systemPharmacy.id,
      value: systemPharmacy.id,
      text: systemPharmacy.name
    }));

    return (
      <Dropdown
        style={{
          margin: '0 10px'
        }}
        onChange={(e, data) => {
          this.setState({
            systemPharmacySelect: data.value
          });
        }}
        placeholder="System Products"
        search
        selection
        value={systemPharmacySelect}
        options={systemProductsOptions}
      />
    );
  };

  dropDownSystemProduct = () => {
    const {
      state: {
        systemProductSelect
      },
      props: {
        systemProducts
      }
    } = this;

    const systemProductsOptions = systemProducts.map(systemProduct => ({
      key: systemProduct.id,
      value: systemProduct.id,
      text: systemProduct.name
    }));

    return (
      <Dropdown
        style={{
          margin: '0 10px'
        }}
        onChange={(e, data) => {
          this.setState({
            systemProductSelect: data.value
          });
        }}
        placeholder="System Products"
        search
        selection
        value={systemProductSelect}
        options={systemProductsOptions}
      />
    );
  };

  handleItemClick = async (e, { name }) => {
    const {
      state: {
        distributorSelect,
        systemProductSelect,
        systemPharmacySelect
      }
    } = this;
    await this.searchShipment({
      distributor: distributorSelect,
      systemProduct: systemProductSelect,
      systemPharmacy: systemPharmacySelect,
      page: name
    });
    this.setState({ activeItem: name });
  };

  pagination = () => {
    const {
      state: {
        countPage,
        activeItem
      }
    } = this;

    const menuItems = [];
    console.log('countPage', countPage);
    for (let i = 1; i < countPage + 1; i += 1) {
      menuItems.push(
        <Menu.Item
          key={i}
          name={`${i}`}
          active={`${activeItem}` === `${i}`}
          onClick={this.handleItemClick}
        />
      );
    }
    return menuItems;
  };

  search = async () => {
    const {
      state: {
        distributorSelect,
        systemProductSelect,
        systemPharmacySelect
      }
    } = this;
    await this.searchShipment({
      distributor: distributorSelect,
      systemProduct: systemProductSelect,
      systemPharmacy: systemPharmacySelect
    });
  };

  render() {
    const {
      state: {
        shipments,
        countPage,

      }
    } = this;

    console.log('shipments', shipments);
    return (
      <div>
        <div
          style={{
            display: 'flex'
          }}
        >
          <Button
            color="red"
            onClick={() => {
              this.setState({
                distributorSelect: null,
                systemProductSelect: null,
                systemPharmacySelect: null
              });
            }}
          >
            Clear
          </Button>
          {this.dropDownDistributors()}
          {this.dropDownSystemProduct()}
          {this.dropDownSystemPharmacy()}
          <Button
            color="blue"
            onClick={this.search}
          >
            Search
          </Button>
        </div>
        {!!shipments.length && <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell textAlign="center">Id</Table.HeaderCell>
              <Table.HeaderCell textAlign="center">Distributor</Table.HeaderCell>
              <Table.HeaderCell textAlign="center">Product</Table.HeaderCell>
              <Table.HeaderCell textAlign="center">Pharmacy</Table.HeaderCell>
              <Table.HeaderCell textAlign="center">Date</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {
              shipments.map(shipment => (<Table.Row
                key={shipment.id}
              >
                <Table.Cell
                  textAlign="center"
                >
                  {shipment.id}
                </Table.Cell>
                <Table.Cell
                  textAlign="center"
                >
                  {shipment.distributor.name}
                </Table.Cell>
                <Table.Cell
                  textAlign="center"
                >
                  {shipment.system_product.name}
                </Table.Cell>
                <Table.Cell
                  textAlign="center"
                >
                  {shipment.system_pharmacy.name}
                </Table.Cell>
                <Table.Cell
                  textAlign="center"
                >
                  {Moment(shipment.date).format('YYYY-MM-DD HH:mm')}
                </Table.Cell>
              </Table.Row>))
            }
          </Table.Body>
        </Table>}
        { !!countPage && <div
          style={{
            margin: 10,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Menu pagination>
            {this.pagination()}
          </Menu>
        </div>
        }
      </div>
    );
  }
}

Shipment.propTypes = {
  match: PropTypes.object,
  distributors: PropTypes.array,
  systemProducts: PropTypes.array,
  systemPharmacies: PropTypes.array,
};

const stateToProps = state => ({
  distributors: state.distributors,
  systemProducts: state.systemProducts,
  systemPharmacies: state.systemPharmacies
});

const actionToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(stateToProps, actionToProps)(Shipment);
