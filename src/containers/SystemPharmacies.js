import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Input } from 'semantic-ui-react';

import SystemPharmaciesTable from '../components/SystemPharmaciesTable';
import ModalWindow from '../components/ModalWindow';

import {
  createSystemPharmacy
} from '../redux/actions/systemPharmacies';

class SystemPharmacies extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
      newSystemPharmacyName: null
    };
  }

  onSave = () => {
    const {
      newSystemPharmacyName
    } = this.state;
    this.props.createSystemPharmacy(newSystemPharmacyName);
    this.setState({
      showModal: false,
      newSystemPharmacyName: null
    });
  };

  closeModal = () => {
    const {
      showModal
    } = this.state;

    if (!showModal) return;
    this.setState({ showModal: false });
  };

  render() {
    const {
      state: {
        showModal
      }
    } = this;

    return (
      <div>
        <div
          style={{
            width: '100%',
            display: 'flex',
            justifyContent: 'flex-end',
            padding: 10
          }}
        >
          <Button
            onClick={() => {
              if (showModal) return;
              this.setState({ showModal: true });
            }}
          >
            Add
          </Button>
        </div>
        <SystemPharmaciesTable />
        <ModalWindow
          title="Create new system pharmacy"
          closeModal={this.closeModal}
          show={showModal}
          onSave={this.onSave}
        >
          <Input
            fluid
            placeholder="System pharmacy name"
            onChange={(event) => {
              const value = event.target.value;
              this.setState({
                newSystemPharmacyName: value
              });
            }}
          />
        </ModalWindow>
      </div>
    );
  }
}

SystemPharmacies.propTypes = {
  createSystemPharmacy: PropTypes.func.isRequired
};

SystemPharmacies.defaultProps = {};

const stateToProps = state => ({});

const actionToProps = dispatch => bindActionCreators({
  createSystemPharmacy
}, dispatch);

export default connect(stateToProps, actionToProps)(SystemPharmacies);
