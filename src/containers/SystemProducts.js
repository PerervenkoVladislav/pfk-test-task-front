import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Input } from 'semantic-ui-react';

import SystemProductsTable from '../components/SystemProductsTable';
import ModalWindow from '../components/ModalWindow';

import {
  createSystemProduct
} from '../redux/actions/systemProducts';

class SystemProducts extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
      newSystemProductName: null
    };
  }

  onSave = () => {
    const {
      newSystemProductName
    } = this.state;
    this.props.createSystemProduct(newSystemProductName);
    this.setState({
      showModal: false,
      newSystemProductName: null
    });
  };

  closeModal = () => {
    const {
      showModal
    } = this.state;

    if (!showModal) return;
    this.setState({ showModal: false });
  };

  render() {
    const {
      state: {
        showModal
      }
    } = this;

    return (
      <div>
        <div
          style={{
            width: '100%',
            display: 'flex',
            justifyContent: 'flex-end',
            padding: 10
          }}
        >
          <Button
            onClick={() => {
              if (showModal) return;
              this.setState({ showModal: true });
            }}
          >
            Add
          </Button>
        </div>
        <SystemProductsTable />
        <ModalWindow
          title="Create new system product"
          closeModal={this.closeModal}
          show={showModal}
          onSave={this.onSave}
        >
          <Input
            fluid
            placeholder="System product name"
            onChange={(event) => {
              const value = event.target.value;
              this.setState({
                newSystemProductName: value
              });
            }}
          />
        </ModalWindow>
      </div>
    );
  }
}

SystemProducts.propTypes = {
  createSystemProduct: PropTypes.func.isRequired
};

SystemProducts.defaultProps = {};

const stateToProps = state => ({});

const actionToProps = dispatch => bindActionCreators({
  createSystemProduct
}, dispatch);

export default connect(stateToProps, actionToProps)(SystemProducts);
