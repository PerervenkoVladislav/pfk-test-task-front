import ActionTypes from '../types';

export function setActiveItemMenu({ activeItemMenu }) {
  return {
    type: ActionTypes.SET_ACTIVE_ITEM_MENU,
    activeItemMenu
  };
}
