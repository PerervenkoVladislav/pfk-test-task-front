import ActionTypes from '../types';

export function getDistributors() {
  return {
    type: ActionTypes.GET_DISTRIBUTORS
  };
}

export function setDistributors({ distributors }) {
  return {
    type: ActionTypes.SET_DISTRIBUTORS,
    distributors
  };
}

export function createDistributor(name) {
  return {
    type: ActionTypes.CREATE_DISTRIBUTOR,
    name
  };
}

export function addNewDistributor({ distributor }) {
  return {
    type: ActionTypes.ADD_NEW_DISTRIBUTOR,
    distributor
  };
}

export function importData({ distributor, file }) {
  return {
    type: ActionTypes.IMPORT_DATA,
    distributor,
    file
  };
}

export function setImportData({ distributor, records }) {
  console.log('setImportData', distributor, records);
  return {
    type: ActionTypes.SET_IMPORT_DATA,
    distributor: distributor.id,
    records
  };
}

export function updateProductImportData({ distributor, product, systemProduct }) {
  console.log('setImportData', distributor, product, systemProduct);
  return {
    type: ActionTypes.UPDATE_PRODUCT_IMPORT_DATA,
    distributor,
    product,
    systemProduct
  };
}

export function updatePharmacyImportData({ distributor, pharmacy, systemPharmacy }) {
  return {
    type: ActionTypes.UPDATE_PHARMACY_IMPORT_DATA,
    distributor,
    pharmacy,
    systemPharmacy
  };
}

export function clearImportDataByDistributors({ distributor }) {
  return {
    type: ActionTypes.CLEAR_IMPORT_DATA,
    distributor
  };
}
