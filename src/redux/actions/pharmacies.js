import ActionTypes from '../types';

export function setPharmacy({ pharmacy }) {
  return {
    type: ActionTypes.SET_PHARMACY,
    pharmacy
  };
}

export function setDistributorPharmacies({ distributor, pharmacies }) {
  console.log('setDistributorPharmacies', distributor, pharmacies);
  return {
    type: ActionTypes.SET_DISTRIBUTOR_PHARMACIES,
    distributor,
    pharmacies
  };
}

export function updatePharmacy({ id, systemPharmacy }) {
  console.log('action updatePharmacies', id, systemPharmacy);
  return {
    type: ActionTypes.UPDATE_PHARMACY,
    id,
    systemPharmacy
  };
}
