import ActionTypes from '../types';

export function setProduct({ product }) {
  return {
    type: ActionTypes.SET_PRODUCT,
    product
  };
}

export function getProducts() {
  console.log('in get products');
  return {
    type: ActionTypes.GET_PRODUCTS
  };
}

export function updateProduct({ id, systemProduct }) {
  console.log('action updateProduct');
  return {
    type: ActionTypes.UPDATE_PRODUCT,
    id,
    systemProduct
  };
}

export function setDistributorProducts({ distributor, products }) {
  return {
    type: ActionTypes.SET_DISTRIBUTOR_PRODUCTS,
    distributor,
    products
  };
}
