import ActionTypes from '../types';

export function importShipment({ distributor, shipments }) {
  return {
    type: ActionTypes.IMPORT_SHIPMENT,
    distributor,
    shipments
  };
}