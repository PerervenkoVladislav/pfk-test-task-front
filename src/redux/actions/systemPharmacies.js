import ActionTypes from '../types';

export function getSystemPharmacies() {
  return {
    type: ActionTypes.GET_SYSTEM_PHARMACIES
  };
}

export function setSystemPharmacies({ systemPharmacies }) {
  console.log('systemPharmacies', systemPharmacies);
  return {
    type: ActionTypes.SET_SYSTEM_PHARMACIES,
    systemPharmacies
  };
}

export function createSystemPharmacy(name) {
  return {
    type: ActionTypes.CREATE_SYSTEM_PHARMACY,
    name
  };
}

export function addNewSystemPharmacy({ systemPharmacy }) {
  return {
    type: ActionTypes.ADD_NEW_SYSTEM_PHARMACY,
    systemPharmacy
  };
}
