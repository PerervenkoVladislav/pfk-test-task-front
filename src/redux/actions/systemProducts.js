import ActionTypes from '../types';

export function getSystemProducts() {
  return {
    type: ActionTypes.GET_SYSTEM_PRODUCTS
  };
}

export function setSystemProducts({ systemProducts }) {
  console.log('systemProducts', systemProducts);
  return {
    type: ActionTypes.SET_SYSTEM_PRODUCTS,
    systemProducts
  };
}

export function createSystemProduct(name) {
  return {
    type: ActionTypes.CREATE_SYSTEM_PRODUCT,
    name
  };
}

export function addNewSystemProduct({ systemProduct }) {
  return {
    type: ActionTypes.ADD_NEW_SYSTEM_PRODUCT,
    systemProduct
  };
}
