import { REHYDRATE } from 'redux-persist/constants';

import settings from '../../settings';
import ActionTypes from '../types';

const initialState = {
  activeItemMenu: settings.itemsMenu[0].item
};

export default function product(state = initialState, action = {}) {
  switch (action.type) {
    case REHYDRATE: {
      return {
        ...action.payload.application
      };
    }

    case ActionTypes.SET_ACTIVE_ITEM_MENU:
      return {
        activeItemMenu: action.activeItemMenu
      };

    default:
      return state;
  }
}
