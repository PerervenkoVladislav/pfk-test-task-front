import { REHYDRATE } from 'redux-persist/constants';

import ActionTypes from '../types';

const initialState = [];

export default function product(state = initialState, action = {}) {
  switch (action.type) {
    // case REHYDRATE: {
    //   console.log('REHYDRATE', action.payload);
    //   return {
    //     data: action.payload.distributors
    //   };
    // }

    case ActionTypes.SET_DISTRIBUTORS:
      return [
        ...action.distributors
      ];

    case ActionTypes.ADD_NEW_DISTRIBUTOR:
      return [
        ...state,
        action.distributor
      ];

    default:
      return state;
  }
}
