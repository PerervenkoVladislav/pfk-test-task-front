import { REHYDRATE } from 'redux-persist/constants';

import ActionTypes from '../types';
import distributors from '../../api/distributors';

const initialState = {};

export default function product(state = initialState, action = {}) {
  switch (action.type) {
    case REHYDRATE: {
      return {
        ...action.payload.importData
      };
    }

    case ActionTypes.SET_IMPORT_DATA:
      console.log('SET_IMPORT_DATA action', action);
      return {
        ...state,
        [action.distributor]: action.records
      };

    case ActionTypes.UPDATE_PRODUCT_IMPORT_DATA: {
      // distributor, product, systemProduct
      const records = state[action.distributor];
      for (let i = 0; i < records.length; i += 1) {
        const products = records[i].products;
        for (let j = 0; j < products.length; j += 1) {
          const product = products[j];
          if (product.current === action.product) {
            products[j] = {
              ...products[j],
              system: action.systemProduct
            };
          }
        }
        records[i] = {
          ...records[i],
          products
        };
      }

      return {
        ...state,
        [action.distributor]: records
      };
    }

    case ActionTypes.UPDATE_PHARMACY_IMPORT_DATA: {
      const {
        distributor,
        pharmacy,
        systemPharmacy
      } = action;
      const records = state[distributor];
      for (let i = 0; i < records.length; i += 1) {
        const record = records[i];
        if (record.pharmacy === pharmacy) {
          records[i] = {
            ...record,
            systemPharmacy
          };
        }
      }

      return {
        ...state,
        [distributor]: records
      };
    }

    case ActionTypes.CLEAR_IMPORT_DATA: {
      const {
        distributor
      } = action;

      return {
        ...state,
        [distributor]: undefined
      };
    }

    default:
      return state;
  }
}
