import products from './products';
import importData from './importData';
import pharmacies from './pharmacies';
import application from './application';
import distributors from './distributors';
import systemProducts from './systemProducts';
import systemPharmacies from './systemPharmacies';

export default {
  products,
  importData,
  pharmacies,
  application,
  distributors,
  systemProducts,
  systemPharmacies
};
