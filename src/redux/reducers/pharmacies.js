import { REHYDRATE } from 'redux-persist/constants';

import ActionTypes from '../types';

const initialState = {};

export default function product(state = initialState, action = {}) {
  switch (action.type) {
    case REHYDRATE: {
      return {
        ...action.payload.pharmacies
      };
    }

    case ActionTypes.SET_PHARMACY: {
      const distributor = action.pharmacy.distributor.id;
      const pharmacies = state[distributor];
      for (let i = 0; i < pharmacies.length; i += 1) {
        if (pharmacies[i].id === action.pharmacy.id) {
          pharmacies[i] = action.pharmacy;
          break;
        }
      }
      return {
        ...state,
        [distributor]: pharmacies
      };
    }

    case ActionTypes.SET_DISTRIBUTOR_PHARMACIES:
      console.log('pharmacies distributor', action.distributor, action.pharmacies);
      return {
        ...state,
        [action.distributor]: action.pharmacies
      };

    default:
      return state;
  }
}
