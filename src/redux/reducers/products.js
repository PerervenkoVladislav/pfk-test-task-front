import { REHYDRATE } from 'redux-persist/constants';

import ActionTypes from '../types';

const initialState = {};

export default function products(state = initialState, action = {}) {
  switch (action.type) {
    case REHYDRATE: {
      return {
        ...action.payload.products
      };
    }

    case ActionTypes.SET_PRODUCT: {
      const distributor = action.product.distributor.id;
      const products = state[distributor];
      for (let i = 0; i < products.length; i += 1) {
        if (products[i].id === action.product.id) {
          products[i] = action.product;
          break;
        }
      }
      return {
        ...state,
        [distributor]: products
      };
    }

    case ActionTypes.SET_DISTRIBUTOR_PRODUCTS:
      return {
        ...state,
        [action.distributor]: action.products
      };

    default:
      return state;
  }
}
