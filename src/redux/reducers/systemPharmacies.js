import { REHYDRATE } from 'redux-persist/constants';

import ActionTypes from '../types';

const initialState = [];

export default function product(state = initialState, action = {}) {
  switch (action.type) {
    // case REHYDRATE: {
    //   console.log('REHYDRATE', action.payload);
    //   return {
    //     data: action.payload.distributors
    //   };
    // }

    case ActionTypes.SET_SYSTEM_PHARMACIES:
      console.log('SET_SYSTEM_PHARMACIES', action);
      return [
        ...action.systemPharmacies
      ];

    case ActionTypes.ADD_NEW_SYSTEM_PHARMACY:
      return [
        ...state,
        action.systemPharmacy
      ];

    default:
      return state;
  }
}
