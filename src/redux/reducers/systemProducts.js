import { REHYDRATE } from 'redux-persist/constants';

import ActionTypes from '../types';

const initialState = [];

export default function product(state = initialState, action = {}) {
  switch (action.type) {
    // case REHYDRATE: {
    //   console.log('REHYDRATE', action.payload);
    //   return {
    //     data: action.payload.distributors
    //   };
    // }

    case ActionTypes.SET_SYSTEM_PRODUCTS:
      console.log('SET_SYSTEM_PRODUCTS', action);
      return [
        ...action.systemProducts
      ];

    case ActionTypes.ADD_NEW_SYSTEM_PRODUCT:
      return [
        ...state,
        action.systemProduct
      ];

    default:
      return state;
  }
}
