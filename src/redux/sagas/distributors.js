import { call, put } from 'redux-saga/effects';
// import { push } from 'react-router-redux';

import API from '../../api';
import {
  setDistributors,
  addNewDistributor,
  setImportData
} from '../actions/distributors';

import {
  setDistributorProducts
} from '../actions/products';

import {
  setDistributorPharmacies
} from '../actions/pharmacies';

export function* getDistributors() {
  console.log('sagas getDistributors');
  const response = yield call(API.distributors.getDistributors);

  if (response.success) {
    yield put(setDistributors({
      distributors: response.distributors
    }));
  }
}


export function* createDistributor({ name }) {
  console.log('sagas createDistributor');
  const response = yield call(API.distributors.createDistributor, { name });

  if (response.success) {
    yield put(addNewDistributor({
      distributor: response.distributor
    }));
  }
}

export function* getDataByDistributorId({ payload }) {
  const result = /distributors\/([\w-]+)/.exec(payload.pathname);
  if (!result || !result[1]) return;

  const response = yield call(API.distributors.getDataByDistributorId, { id: result[1] });

  if (response.success) {
    console.log('getDataByDistributorId', response);
    yield put(setDistributorPharmacies({
      distributor: result[1],
      pharmacies: response.pharmacies
    }));
    yield put(setDistributorProducts({
      distributor: result[1],
      products: response.products
    }));
  }
}

export function* importData({ distributor, file }) {
  console.log('sagas importData');
  const response = yield call(API.distributors.importData, { distributor, file });

  console.log('response importData', response);
  if (response.success) {
    console.log('success');
    yield put(setImportData({
      distributor: response.distributor,
      records: response.products
    }));
  }
  return false;
}
