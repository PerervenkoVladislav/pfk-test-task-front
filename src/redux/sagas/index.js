import { fork, takeEvery } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';

import types from '../types';

import {
  updateProduct
} from './products';

import {
  updatePharmacy
} from "./pharmacy";

import {
  importShipment
} from './shipment';

import {
  getDistributors,
  createDistributor,
  getDataByDistributorId,
  importData
} from '../sagas/distributors';

import {
  getSystemProducts,
  createSystemProduct
} from '../sagas/systemProducts';

import {
  getSystemPharmacies,
  createSystemPharmacy
} from '../sagas/systemPharmacies';

function* startup() {
  // get init data
  yield fork(getDistributors);
  yield fork(getSystemProducts);
  yield fork(getSystemPharmacies);
  // yield take([types.GET_DISTRIBUTORS]);
  // const distributors = yield call(API.distributors.getDistributors);
}

export default function* rootSaga() {
  yield fork(startup);
  yield takeEvery([types.IMPORT_DATA], importData);
  yield takeEvery([types.UPDATE_PRODUCT], updateProduct);
  yield takeEvery([types.UPDATE_PHARMACY], updatePharmacy);
  yield takeEvery([types.IMPORT_SHIPMENT], importShipment);
  yield takeEvery([LOCATION_CHANGE], getDataByDistributorId);
  yield takeEvery([types.CREATE_DISTRIBUTOR], createDistributor);
  yield takeEvery([types.CREATE_SYSTEM_PRODUCT], createSystemProduct);
  yield takeEvery([types.CREATE_SYSTEM_PHARMACY], createSystemPharmacy);
  // yield takeEvery([types.GET_DISTRIBUTORS], getDistributors);
}
