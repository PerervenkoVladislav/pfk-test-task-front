import { call, put } from 'redux-saga/effects';
// import { push } from 'react-router-redux';

import API from '../../api';
import {
  setPharmacy
} from '../actions/pharmacies';

export function* updatePharmacy({ id, systemPharmacy }) {
  const response = yield call(API.pharmacies.updatePharmacy, { id, systemPharmacy });

  if (response.success) {
    yield put(setPharmacy({
      pharmacy: response.pharmacy
    }));
  }
}
