import { call, put } from 'redux-saga/effects';
// import { push } from 'react-router-redux';

import API from '../../api';
import {
  setProduct
} from '../actions/products';

export function* getProducts() {
  const response = yield call(API.products.getProducts);

  if (response.success) {
    yield put(setProduct({
      products: response.products
    }));
  }
}

export function* updateProduct({ id, systemProduct }) {
  const response = yield call(API.products.updateProduct, { id, systemProduct });

  if (response.success) {
    yield put(setProduct({
      product: response.product
    }));
  }
}
