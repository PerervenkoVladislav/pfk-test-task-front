import { call, put } from 'redux-saga/effects';

import API from '../../api';

import {
  clearImportDataByDistributors
} from '../actions/distributors';

export function* importShipment({ distributor, shipments }) {
  console.log('sagas importData', distributor, shipments);
  const response = yield call(API.shipment.importShipment, { distributor, shipments });

  console.log('response', response);
  if (response.success) {
    yield put(clearImportDataByDistributors({
      distributor
    }));
  }
  return false;
}
