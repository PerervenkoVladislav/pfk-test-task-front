import { call, put } from 'redux-saga/effects';
// import { push } from 'react-router-redux';

import API from '../../api';
import {
  setSystemPharmacies,
  addNewSystemPharmacy
} from '../actions/systemPharmacies';

export function* getSystemPharmacies() {
  console.log('sagas getSystemPharmacies');
  const response = yield call(API.systemPharmacies.getSystemPharmacies);

  if (response.success) {
    yield put(setSystemPharmacies({
      systemPharmacies: response.systemPharmacies
    }));
  }
}


export function* createSystemPharmacy({ name }) {
  console.log('sagas createSystemProduct');
  const response = yield call(API.systemPharmacies.createSystemPharmacy, { name });

  if (response.success) {
    yield put(addNewSystemPharmacy({
      systemPharmacy: response.systemPharmacy
    }));
  }
}
