import { call, put } from 'redux-saga/effects';
// import { push } from 'react-router-redux';

import API from '../../api';
import {
  setSystemProducts,
  addNewSystemProduct
} from '../actions/systemProducts';

export function* getSystemProducts() {
  console.log('sagas getSystemProducts');
  const response = yield call(API.systemProducts.getSystemProducts);

  if (response.success) {
    yield put(setSystemProducts({
      systemProducts: response.systemProducts
    }));
  }
}


export function* createSystemProduct({ name }) {
  console.log('sagas createSystemProduct');
  const response = yield call(API.systemProducts.createSystemProduct, { name });

  if (response.success) {
    yield put(addNewSystemProduct({
      systemProduct: response.systemProduct
    }));
  }
}
