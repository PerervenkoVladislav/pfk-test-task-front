import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { autoRehydrate } from 'redux-persist';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import rootSaga from './sagas';
import reducers from './reducers';

export const history = createHistory();

export default function configureStore() {
  const sagaMiddleware = createSagaMiddleware();
  // const middleware = routerMiddleware(history);
  const middleware = [sagaMiddleware, routerMiddleware(history)];

  const store = createStore(
    combineReducers({
      ...reducers,
      router: routerReducer
    }),
    compose(
      applyMiddleware(...middleware),
      autoRehydrate()
    )
  );
  sagaMiddleware.run(rootSaga);

  return store;
}
