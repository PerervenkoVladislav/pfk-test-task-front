const ActionTypes = {
  // products
  SET_PRODUCT: 'SET_PRODUCT',
  GET_PRODUCTS: 'GET_PRODUCTS',
  UPDATE_PRODUCT: 'UPDATE_PRODUCT',
  SET_DISTRIBUTOR_PRODUCTS: 'SET_DISTRIBUTOR_PRODUCTS',

  // pharmacies
  SET_PHARMACY: 'SET_PHARMACY',
  SET_DISTRIBUTOR_PHARMACIES: 'SET_DISTRIBUTOR_PHARMACIES',
  UPDATE_PHARMACY: 'UPDATE_PHARMACY',

  // application
  SET_ACTIVE_ITEM_MENU: 'SET_ACTIVE_ITEM_MENU',

  // distributors
  GET_DISTRIBUTORS: 'GET_DISTRIBUTORS',
  SET_DISTRIBUTORS: 'SET_DISTRIBUTORS',
  CREATE_DISTRIBUTOR: 'CREATE_DISTRIBUTOR',
  ADD_NEW_DISTRIBUTOR: 'ADD_NEW_DISTRIBUTOR',

  // system products
  GET_SYSTEM_PRODUCTS: 'GET_SYSTEM_PRODUCTS',
  SET_SYSTEM_PRODUCTS: 'SET_SYSTEM_PRODUCTS',
  CREATE_SYSTEM_PRODUCT: 'CREATE_SYSTEM_PRODUCT',
  ADD_NEW_SYSTEM_PRODUCT: 'ADD_NEW_SYSTEM_PRODUCT',

  // system pharmacy
  GET_SYSTEM_PHARMACIES: 'GET_SYSTEM_PHARMACIES',
  SET_SYSTEM_PHARMACIES: 'SET_SYSTEM_PHARMACIES',
  CREATE_SYSTEM_PHARMACY: 'CREATE_SYSTEM_PHARMACY',
  ADD_NEW_SYSTEM_PHARMACY: 'ADD_NEW_SYSTEM_PHARMACY',

  // import data
  IMPORT_DATA: 'IMPORT_DATA',
  SET_IMPORT_DATA: 'SET_IMPORT_DATA',
  UPDATE_PRODUCT_IMPORT_DATA: 'UPDATE_PRODUCT_IMPORT_DATA',
  UPDATE_PHARMACY_IMPORT_DATA: 'UPDATE_PHARMACY_IMPORT_DATA',
  CLEAR_IMPORT_DATA: 'CLEAR_IMPORT_DATA',

  // shipments
  IMPORT_SHIPMENT: 'IMPORT_SHIPMENT',
  GET_SHIPMENTS: 'GET_SHIPMENTS'
};

export default ActionTypes;
