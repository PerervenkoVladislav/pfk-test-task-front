export default {
  itemsMenu: [
    { name: 'Shipment', item: 'shipment', link: '' },
    { name: 'Distributors', item: 'distributors', link: 'distributors' },
    { name: 'System products', item: 'system_products', link: 'system_products' },
    { name: 'System pharmacy', item: 'system_pharmacy', link: 'system_pharmacy' },
    { name: 'Import data', item: 'import_data', link: 'import_data' }
  ]
};
