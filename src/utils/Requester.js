import { create, DEFAULT_HEADERS } from 'apisauce';

const Requester = create({
  baseURL: 'http://127.0.0.1:8000',
  headers: DEFAULT_HEADERS
});

export default Requester;
