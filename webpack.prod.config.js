const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = env => ({
  context: __dirname,
  entry: [
    path.resolve(__dirname, './src/app.jsx'),
    'babel-polyfill',
    'whatwg-fetch'
  ],
  output: {
    path: __dirname,
    filename: 'public/bundle.js'
  },
  module: {
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        include: [
          path.resolve(__dirname, 'src')
        ],
        query: {
          presets: ['es2015', 'stage-0', 'react', 'react-hmre'],
          plugins: ['transform-decorators-legacy', ['transform-runtime',
            {
              helpers: false,
              polyfill: false,
              regenerator: true
            }
          ]]
        }
      },
      {
        test: /\.s?css$/,
        include: [
          path.resolve(__dirname, 'scss'),
          path.resolve(__dirname, 'node_modules/semantic-ui-css'),
        ],
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' },
        ]
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/,
        include: [
          path.resolve(__dirname, 'src/img'),
          path.resolve(__dirname, 'node_modules/semantic-ui-css'),
        ],
        loader: 'file-loader?name=img/[name].[ext]'
      },
      {
        test: /\.(eot|svg|ttf|otf|woff|woff2)$/,
        include: [
          path.resolve(__dirname, 'src/font'),
          path.resolve(__dirname, 'node_modules/semantic-ui-css'),
        ],
        loader: 'file-loader?name=fonts/[name].[ext]'
      },
    ]
  },
  devtool: 'source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: JSON.stringify(env) }
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      inject: false,
      template: 'index.html'
    }),
  ]
});
